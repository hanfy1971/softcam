#!/bin/sh

status='/var/lib/opkg/status'
package='enigma2-plugin-softcams-ncam

if grep -q $package $status; then
echo "> removing package please wait..."
sleep 3s
opkg remove $package

else

#remove unnecessary files and folders
if [  -d "/CONTROL" ]; then
rm -r  /CONTROL
fi
rm -rf /control
rm -rf /postinst
rm -rf /preinst
rm -rf /prerm
rm -rf /postrm
rm -rf /tmp/*.deb
rm -rf /tmp/*.tar.gz

#config
pack=ncam
version=V14.7-r0
ipk="$pack-$version.deb"
install="dpkg -i --force-overwrite /tmp/*.deb"

# Download and install plugin
echo "> Downloading "$pack"-"$version" please wait..."
sleep 3s

cd /tmp
set -e
wget --show-progress https://gitlab.com/hanfy1971/softcam/-/raw/main/ncam_V14.7-r0_all.deb
$install $deb
set +e
cd ..
wait
rm -f /tmp/$deb

if [ $? -eq 0 ]; then
echo "> "$pack"-"$version" installed successfully"
sleep 3s
else
echo " installation failed"
fi

fi
exit 0

gggg

